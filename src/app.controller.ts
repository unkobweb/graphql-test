import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';

@Controller()
export class AppController {

  @Get()
  getHello(): any {
    return 'Hello world'
  }
}
