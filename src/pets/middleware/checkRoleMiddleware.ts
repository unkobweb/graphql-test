import { ForbiddenException } from "@nestjs/common";
import { FieldMiddleware, MiddlewareContext, NextFn } from "@nestjs/graphql";

export const checkRoleMiddleware: FieldMiddleware = async (
    ctx: MiddlewareContext,
    next: NextFn,
  ) => {
    const { info } = ctx;
    const { extensions } = info.parentType.getFields()[info.fieldName];
    /**
     * In a real-world application, the "userRole" variable
     * should represent the caller's (user) role (for example, "ctx.user.role").
     */
    if (extensions.role && ctx.context.req.user.role !== extensions.role) {
      // or just "return null" to ignore
      const type = typeof await next()
      switch (type) {
          case "string":
              return ''
              break;
          case "number":
              return NaN
              break;
      }
    }
    return next();
  };