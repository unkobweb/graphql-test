import { Extensions, Field, Int, ObjectType } from "@nestjs/graphql";
import { Owner } from "src/owners/owner.entity";
import { ROLES } from "src/users/user.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { checkRoleMiddleware } from "./middleware/checkRoleMiddleware";

@Entity()
@ObjectType()
export class Pet {
    @PrimaryGeneratedColumn('uuid', {name: 'uuid'})
    @Field()
    uuid: string;

    @Column()
    @Field()
    name: string;

    @Column({nullable: true})
    @Field({nullable: true})
    type?: string;

    @Column()
    @Field()
    ownerUuid: string;

    @ManyToOne(() => Owner, owner => owner.pets)
    @Field(type => Owner)
    owner: Owner
}