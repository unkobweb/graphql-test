import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { CurrentUser } from 'src/auth/current-user.decorator';
import { GqlAuthGuard } from 'src/auth/gql-auth.guard';
import { Owner } from 'src/owners/owner.entity';
import { User } from 'src/users/user.entity';
import { CreatePetDto } from './dto/create-pet.dto';
import { Pet } from './pet.entity';
import { PetsService } from './pets.service';

@UseGuards(GqlAuthGuard)
@Resolver(of => Pet)
export class PetsResolver {
    constructor(private petsService: PetsService) {}

    @Query(returns => [Pet], {name: "pets"})
    pets(@CurrentUser() user: User) {
        console.log(user)
        return this.petsService.findAll()
    }

    @Query(returns => Pet, {name: "pet"})
    getPet(@Args('uuid') uuid: string): Promise<Pet> {
        return this.petsService.findOne(uuid)
    }

    @ResolveField(returns => Owner)
    owner(@Parent() pet: Pet): Promise<Owner>{
        return this.petsService.getOwner(pet.ownerUuid)
    }

    @Mutation(returns => Pet)
    createPet(@Args('createPetDto') createPetDto: CreatePetDto): Promise<Pet> {
        return this.petsService.createPet(createPetDto)
    }
}
