import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Owner } from 'src/owners/owner.entity';
import { OwnersService } from 'src/owners/owners.service';
import { Repository } from 'typeorm';
import { CreatePetDto } from './dto/create-pet.dto';
import { Pet } from './pet.entity';

@Injectable()
export class PetsService {
    constructor(
        @InjectRepository(Pet) private repo: Repository<Pet>,
        private ownerService: OwnersService
    ) {}

    createPet(createPetDto: CreatePetDto): Promise<Pet> {
        return this.repo.save(createPetDto)
    }

    findAll(): Promise<Pet[]> {
        return this.repo.find();
    }

    findOne(uuid: string): Promise<Pet> {
        return this.repo.findOneOrFail(uuid)
    }

    getOwner(ownerUuid: string): Promise<Owner> {
        return this.ownerService.findOne(ownerUuid)
    }
}
