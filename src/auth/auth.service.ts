import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { FindOneOptions } from 'typeorm';
import * as argon2 from "argon2";
import { JwtService } from '@nestjs/jwt';
import { LoginInput } from './dto/login.input';

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService, private jwtService: JwtService) {}

    async validateUser(username: string, password: string): Promise<any> {
        try {
            const opts: FindOneOptions = {
                where: {
                    email: username
                }
            }
            const user = await this.usersService.findOne(opts);
            if (user && await argon2.verify(user.password, password)){
                const {password, ...rest} = user;
                return rest;
            }
            return null
        } catch (e) {
            throw new UnauthorizedException()            
        }
    }

    async login(loginDto: LoginInput){
        const user = await this.validateUser(loginDto.username, loginDto.password)
        const payload = {
            email: user.email,
            role: user.role,
            sub: user.uuid
        }

        return {
            access_token: this.jwtService.sign(payload)
        }
    }
}
