import { Query, UseGuards } from '@nestjs/common';
import { Args, Field, Mutation, ObjectType, Resolver } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { LoginInput } from './dto/login.input';
import { LocalAuthGuard } from './local-auth.guard';

@ObjectType()
class Token {
    @Field()
    access_token: string
}

@Resolver(() => Token)
export class AuthResolver {
    constructor(private readonly authService: AuthService) {}

    @Mutation(() => Token)
    async login(@Args('loginDto') loginDto: LoginInput){
        return await this.authService.login(loginDto)
    }
}
