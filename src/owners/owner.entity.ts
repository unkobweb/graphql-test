import { ObjectType, Field, Int, Extensions } from '@nestjs/graphql';
import { Pet } from 'src/pets/pet.entity';
import { ROLES } from 'src/users/user.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
@ObjectType()
export class Owner {
  @PrimaryGeneratedColumn('uuid', {name: 'uuid'})
  @Field()
  @Extensions({role: ROLES.ADMIN})
  uuid: string;

  @Field()
  @Column()
  name: string

  @OneToMany(() => Pet, pet => pet.owner)
  @Field(type => [Pet], {nullable: true})
  pets?: Pet[]
}
