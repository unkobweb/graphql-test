import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateOwnerInput } from './dto/create-owner.input';
import { UpdateOwnerInput } from './dto/update-owner.input';
import { Owner } from './owner.entity';

@Injectable()
export class OwnersService {
  constructor(@InjectRepository(Owner) private repo: Repository<Owner>) {}
  create(createOwnerInput: CreateOwnerInput) {
    return this.repo.save(createOwnerInput);
  }

  findAll() {
    return this.repo.find();
  }

  findOne(uuid: string) {
    return this.repo.findOne(uuid);
  }

  update(id: number, updateOwnerInput: UpdateOwnerInput) {
    return `This action updates a #${id} owner`;
  }

  remove(id: number) {
    return `This action removes a #${id} owner`;
  }
}
