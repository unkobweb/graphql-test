import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Owner } from 'src/owners/owner.entity';
import { Pet } from 'src/pets/pet.entity';
import { User, ROLES } from 'src/users/user.entity';
import { Repository } from 'typeorm';
import * as argon2 from "argon2";

@Injectable()
export class FixturesService implements OnModuleInit {
  constructor(
    @InjectRepository(Owner) private readonly ownerRepo: Repository<Owner>,
    @InjectRepository(Pet) private readonly petRepo: Repository<Pet>,
    @InjectRepository(User) private readonly userRepo: Repository<User>,
  ) {}
  
  async generate() {
    const users = await this.userRepo.find()
    const pets = await this.petRepo.find()
    const owner = await this.ownerRepo.find()

    const userData = [
      {
        email: "alexandre@beehelp.fr",
        password: "Test4444",
        role: "ADMIN"
      }
    ]

    if (users.length === 0) {
      for(const user of userData) {
        const newUser = new User()
        newUser.email = user.email;
        newUser.password = await argon2.hash(user.password)
        newUser.role = ROLES.ADMIN
        await this.userRepo.save(newUser)
      }
    }
  }
  
  onModuleInit() {
    this.generate()  
  }
}
