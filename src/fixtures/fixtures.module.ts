import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Owner } from 'src/owners/owner.entity';
import { Pet } from 'src/pets/pet.entity';
import { User } from 'src/users/user.entity';
import { FixturesService } from './fixtures.service';

@Module({
  imports: [TypeOrmModule.forFeature([User,Owner,Pet])],
  providers: [FixturesService]
})
export class FixturesModule {}
