import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User } from './user.entity';
import * as argon2 from "argon2";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly repo: Repository<User>
  ) {}

  async create(createUserInput: CreateUserInput) {
    const user = this.repo.create(createUserInput)
    user.password = await argon2.hash(user.password)
    return this.repo.save(user);
  }

  findAll(options?:FindManyOptions) {
    return this.repo.find(options);
  }

  findOne(option: FindOneOptions) {
    return this.repo.findOne(option);
  }

  update(uuid: string, updateUserInput: UpdateUserInput) {
    return this.repo.update(uuid, updateUserInput);
  }

  remove(uuid: string) {
    return this.repo.delete(uuid);
  }
}
