import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export enum ROLES {
  ADMIN = "ADMIN",
  USER = "USER"
}

@Entity()
@ObjectType()
export class User {
  @PrimaryGeneratedColumn('uuid', {name: 'uuid'})
  @Field()
  uuid: string;

  @Column()
  @Field()
  email: string;

  @Column()
  @Field()
  password: string;

  @Column()
  @Field()
  role: ROLES;
}
