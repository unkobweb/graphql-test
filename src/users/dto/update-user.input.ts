import { CreateUserInput } from './create-user.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

enum ROLES {
  ADMIN = "ADMIN",
  USER = "USER"
}

@InputType()
export class UpdateUserInput extends PartialType(CreateUserInput) {
  @Field()
  uuid: string;

  @Field()
  email: string;

  @Field()
  role: ROLES;
}
