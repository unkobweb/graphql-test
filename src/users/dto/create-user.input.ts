import { InputType, Int, Field } from '@nestjs/graphql';

enum ROLES {
  ADMIN = "ADMIN",
  USER = "USER"
}

@InputType()
export class CreateUserInput {
  @Field()
  email: string;

  @Field()
  password: string;

  @Field()
  role: ROLES;
}
