import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PetsModule } from './pets/pets.module';
import { OwnersModule } from './owners/owners.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { FixturesModule } from './fixtures/fixtures.module';
import { checkRoleMiddleware } from './pets/middleware/checkRoleMiddleware';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(),'src/schema.gql'),
      buildSchemaOptions: {
        fieldMiddleware: [checkRoleMiddleware]
      }
    }),
    TypeOrmModule.forRoot({
      charset: "utf8mb4",
      type: 'mysql',
      host: "127.0.0.1",
      port: 3306,
      username: "root",
      password: "root",
      database: "graphql",
      entities: [
        "dist/**/*.entity{.ts,.js}"
      ],
      synchronize: true,
      logging: true,
    }),
    PetsModule,
    OwnersModule,
    UsersModule,
    AuthModule,
    FixturesModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
